import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class EchoClient {
    private DatagramSocket socket;
    private InetAddress address;
    private InetAddress local;

    private byte[] buffer;



    public EchoClient() throws UnknownHostException, SocketException {
        //local = InetAddress.getByName("192.168.30.13");
        socket = new DatagramSocket(10);
        //address = InetAddress.getByName("192.168.30.10");
        address = InetAddress.getByName("192.168.30.26");
        buffer = new byte[1024];
    }

    public void sendEcho(String path) throws IOException {

        File file = new File(path);
        byte[] bytesArray = new byte[(int) file.length()];

        FileInputStream fis = new FileInputStream(file);
        fis.read(bytesArray);
        fis.close();

        DatagramPacket packet;

        buffer = (path).getBytes();
        packet = new DatagramPacket(buffer, buffer.length, address, 11);
        socket.send(packet);

        buffer = String.valueOf(bytesArray.length).getBytes();
        packet = new DatagramPacket(buffer, buffer.length, address, 11);
        socket.send(packet);

        int offset = 0;
        int length = bytesArray.length;
        byte packetIndex = 1;
        byte[] bufferArray = new byte[1024];
        boolean repeat = false;

        //DPacket for receiving ACK packets
        byte[] ack = new byte[1];
        DatagramPacket ackPacket = new DatagramPacket(ack, 1);

        //set timeout duration
        socket.setSoTimeout(4000);

        while(length > 0) {
            int top;
            if (length >= 1023) {
                top = 1023;
            } else {
                top = length;
            }

            bufferArray[0] = packetIndex;
            top += offset;

            int bufferIndex = 1;

            //get data from bytesArray
            for (int i = offset; i < top-8; i++) {
                bufferArray[bufferIndex] = bytesArray[i];
                offset++;
                bufferIndex++;
            }

            Checksum checksum = new CRC32();
            checksum.update(bufferArray);
            long checksumValue = checksum.getValue();

            byte[] longByte = longToBytes(checksumValue);

            for (int i = 0; i < 8; i++){
                bufferArray[bufferIndex] = longByte[i];
                bufferIndex++;
            }

            packet = new DatagramPacket(bufferArray, 1024, address, 11);
            socket.send(packet);

            repeat = true;
            while (repeat) {
                try {
                    socket.receive(ackPacket);

                    byte msg = ackPacket.getData()[0];
                    if (msg == packetIndex) {
                        if (packetIndex == 127) {
                            packetIndex = 1;
                        } else {
                            packetIndex++;
                        }

                        repeat = false;
                        break;
                    }

                    socket.send(packet);
                    //repeat = false;
                } catch (SocketTimeoutException e) {
                    System.out.println("dwdw");
                    socket.send(packet);
                }
            }

            /*if (length >= 1024) {
                packet = new DatagramPacket(bytesArray, offset, 1024, address, 10);
                socket.send(packet);

                offset+=1024;
                length-=1024;

            } else if (length < 1024) {
                packet = new DatagramPacket(bytesArray, offset, length, address, 10);
                offset += length;
                length -= length;
                socket.send(packet);
            }*/
        }

        packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        String response = new String(packet.getData(), 0, packet.getLength());

        System.out.println(response);


    }

    public void close() {
        socket.close();
    }

    public byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

}

