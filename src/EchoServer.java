import javax.xml.crypto.Data;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class EchoServer extends Thread {

    //30.25
    private DatagramSocket socket;
    private DatagramSocket ackSocket;
    private boolean running;
    private byte[] buffer = new byte[1024];
    private InetAddress address;


    public EchoServer() throws SocketException, UnknownHostException {
        this.socket = new DatagramSocket(10);

        address = InetAddress.getByName("127.0.0.1");
    }

    public void run() {
        running = true;
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

        try {
            socket.receive(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //String fileName = "shit1.png";
        String fileName = new String(packet.getData(), 0, packet.getLength());
        System.out.println(fileName);
        int len = 0;

        try {
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String dataLength = new String(packet.getData(), 0, packet.getLength());
            len = Integer.parseInt(dataLength);

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        File file = new File(fileName);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream str = null;
        try {
            str = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        try {
            socket.setSoTimeout(5000);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        int offset = 0;
        int received = 0;
        byte receivedIndex = 0;
        int length = len;

        byte[] ackT = new byte[1];

        System.out.println(len + "\n");
        while(length > 0) {
            try {
                socket.receive(packet);
                Checksum checksum = new CRC32();


                byte[] CRCCheck = new byte[1016];

                //Read all ze data from ze file exluding ze last 8 bytes which are CRC checks
                for (int i=1; i < packet.getLength()-8; i++){
                    CRCCheck[i] = packet.getData()[i];
                }

                //Checks if the sent data is right
                checksum.update(CRCCheck);
                byte [] ClientChecksum = new byte[8];
                int index = 0;

                for (int i = 1016; i < 1024; i++){
                    ClientChecksum[index] = packet.getData()[i];
                    index++;
                }

                long ClientLongChecksum = bytesToLong(ClientChecksum);


                if (packet.getData()[0] == receivedIndex) {
                    System.out.println("Cyka");
                    ackT[0] = -1;
                    DatagramPacket ack = new DatagramPacket(ackT, 1, address, 101);
                    socket.send(ack);
                    continue;
                }

                if (checksum.getValue() == ClientLongChecksum){
                    System.out.println("smrdis\n");
                    ackT[0] = -1;
                    DatagramPacket ack = new DatagramPacket(ackT, 1, address, 101);
                    socket.send(ack);
                }


                str.write(packet.getData(), 1, packet.getLength()-9);

                if (packet.getLength() == 1024) {
                    length-= 1024;
                    received+= 1024;
                } else {
                    length -= packet.getLength();
                    received += packet.getLength();
                }

                receivedIndex = packet.getData()[0];
                System.out.println(receivedIndex);
                ackT[0] = receivedIndex;
                DatagramPacket ack = new DatagramPacket(ackT, 1, address, 101);
                socket.send(ack);
            } catch (IOException e) {
                ackT[0] = -1;
                DatagramPacket ack = new DatagramPacket(ackT, 1, address, 101);
                try {
                    socket.send(ack);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

        }
        /*while(length > 0) {
            try {
                socket.receive(packet);
                str.write(packet.getData(), 0, packet.getLength());

                if (packet.getLength() == 1024) {
                    length-= 1024;
                    received+= 1024;
                } else {
                    length -= packet.getLength();
                    received += packet.getLength();
                }
            } catch (IOException e) {
                //e.printStackTrace();
                buffer = "Error".getBytes();
                DatagramPacket response = new DatagramPacket(buffer, buffer.length);

                try {
                    socket.send(response);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        }*/


        try {
            str.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        socket.close();
    }

    public long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(bytes);
        buffer.flip();//need flip
        return buffer.getLong();
    }
}
